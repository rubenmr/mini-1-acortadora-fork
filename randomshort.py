#!/usr/bin/python3

import webapp

formulario = """
    <form action= " " method = "POST">
    <p>URL </p>
    <input type="text" name ="url">
    <p>Short </p>
    <input type="text" name="short">
    <input type="submit" value="Enviar">
    </form>
    """
class Shortener (webapp.webApp):

    # Declare and initialize content
    dic_urls = {}

    def parse(self, request):
        metodo = request.split(' ', 2)[0]
        recurso = request.split(' ', 2)[1]
        cuerpo = request.split('\r\n\r\n', 1)[1]

        return metodo, recurso, cuerpo

    def process(self, resourcename):

        metodo, recurso, cuerpo = resourcename

        if metodo == 'GET':
            if recurso == "/":
                httpCode = "200 OK"
                htmlBody = "<html>" + "<body>" + formulario + "<Tiene almacenadas las siguientes urls: >" + "<p>Lista urls: </p>" + str(self.dic_urls) + "</body>" + "</body>"
            else:
                short = recurso.split("/")[1]
                if short in self.dic_urls:
                    url = self.dic_urls[short]
                    httpCode = "302 Found"
                    htmlBody = "<html><body><meta http-equiv='refresh' content='0; URL=" + url + "'></body></html>"
                else:
                    httpCode = "404 Not fount"
                    htmlBody = "<html>" + "<body>" + "<p>RECURSO NO DISPONIBLE</p>" + "</body>" + "</html>"

        elif metodo == 'POST':
            url = (cuerpo.split('&')[0].split('=')[-1])
            short = (cuerpo.split('&')[-1].split('=')[-1])
            if url == "" or short == "":
                httpCode = "404 Not found"
                htmlBody = "<html>" + "<body>" + "<p>ERROR. Por favor, complete los dos campos</p>" + formulario + "<p>Lista urls: </p>" + str(self.dic_urls) + "</body>" + "</body>"
            else:
                if not url.startswith('http://') and not url.startswith('https://'):
                    url = "https://" + url
                self.dic_urls[short] = url
                httpCode = "200 OK"
                htmlBody = "<html><body><ul><li><p>Url cortada: </p><a href=" + url + ">" + short + "</a> " \
                            "<p>Url completa:</p><a href=" + self.dic_urls[short] + ">" + self.dic_urls[short] + "</a></li></ul></body></html>"
        else:
            httpCode = "404 Not Found"
            htmlBody = "Recurso no encontrado"

        return (httpCode, htmlBody)

if __name__ == "__main__":
    testWebApp = Shortener("localhost", 1234)
